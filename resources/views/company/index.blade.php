@extends('admin_panel_master')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    @can('create', App\Company::class)
        <form action="" class="text-left">
            <a href="/companies/create ">
                <button type="button" class="btn btn-success">+</button>
            </a>
        </form>
    @endcan

    <hr>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">name</th>
            <th scope="col">email</th>
            <th scope="col">logo_path</th>
            <th scope="col">website</th>
            <th scope="col">created_at</th>
            <th scope="col">updated_at</th>
            <th scope="col">operation</th>
        </tr>
        </thead>
        <tbody>
        @foreach($companies as $company)

            <?php
            $v = new Verta();
            $created_at = Verta::instance($company->created_at);
            $updated_at = Verta::instance($company->updated_at);

            ?>
            <tr>
                <td scope="row">{{$company->id}}</td>
                <td><a href="/companies/{{$company->id}}">{{$company->name}}</a></td>
                <td>{{$company->email}}</td>
                              <td>
                    <img src="{{$company->logo_path}}" alt="logo_path" width="100px">
                </td>
                <td>{{$company->website}}</td>
                <td>{{$created_at}}</td>
                <td>{{$updated_at}}</td>
                <td>
                    @can('update', $company)
                        <a href="/companies/{{$company->id}}/edit">ویرایش</a>||
                    @endcan
                    @can('delete', $company)
                        <form action="companies/{{ $company->id }}" method="post">
                            @csrf
                            {{ method_field('delete') }}
                            <button class="btn btn-default" type="submit">حذف</button>
                        </form>
                    @endcan
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>


    {{ $companies->links() }}

@endsection




