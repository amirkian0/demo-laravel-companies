@extends('admin_panel_master')

@section('content')

    @if (count($errors) > 0)
        <div class = "alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => 'companies','method' => 'post','files' => true]) !!}
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', 'name')}}
                        {{Form::text('name', '',['class' => 'form-control','placeholder'=>'عنوان را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'email')}}
                        {{Form::email('email', '',['class' => 'form-control','placeholder'=>'ایمیل را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('logo_path', 'logo_path')}}
                        {{Form::file('logo_path',['class' => 'form-control'])}}

                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'website')}}
                        {{Form::text('website', '',['class' => 'form-control','placeholder'=>'آدرس سایت را وارد کنید'])}}
                    </div>

                    {{Form::submit('new company')}}
                </div>
            </div>
        </div>

    </div>

    {!! Form::close() !!}


@endsection
