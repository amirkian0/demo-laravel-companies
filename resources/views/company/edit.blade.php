@extends('admin_panel_master')

@section('content')

    @if (count($errors) > 0)
        <div class = "alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => '/companies/'.$company->id,'method' => 'PUT','files' => true]) !!}
    {{ Form::hidden('id', $company->id) }}
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('name', 'name')}}
                        {{Form::text('name', $company->name,['class' => 'form-control','placeholder'=>'نام را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'email')}}
                        {{Form::email('email', $company->name,['class' => 'form-control','placeholder'=>'ایمیل را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('logo_path', 'logo_path')}}
                        <img src="/{{$company->logo_path}}" class="" alt="intro path" width="100px">
                        {{Form::file('logo_path',['class' => 'form-control'])}}

                    </div>
                    <div class="form-group">
                        {{ Form::label('website', 'website')}}
                        {{Form::text('website', $company->website,['class' => 'form-control','placeholder'=>'وبسایت را وارد کنید'])}}
                    </div>


                    {{Form::submit('Update company')}}
                </div>
            </div>
        </div>

    </div>

    {!! Form::close() !!}


@endsection
