@extends('admin_panel_master')

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
    @endif

    <form action="">
        <a href="/articles/create "><button type="button" class="btn btn-success">+</button></a>
    </form>
    <hr>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">first_name</th>
            <th scope="col">last_name</th>
            <th scope="col">email</th>
            <th scope="col">phone</th>
            <th scope="col">company_id</th>
            <th scope="col">created_at</th>
            <th scope="col">updated_at</th>
            <th scope="col">operation</th>
        </tr>
        </thead>
        <tbody>

            <?php
            $v = new Verta();
            $created_at = Verta::instance($employee->created_at);
            $updated_at = Verta::instance($employee->updated_at);

            ?>
            <tr>
                <td scope="row">{{$employee->id}}</td>
                <td><a href="/employees/{{$employee->id}}">{{$employee->first_name}}</a></td>
                <td scope="row">{{$employee->last_name}}</td>
                <td scope="row">{{$employee->email}}</td>
                <td scope="row">{{$employee->phone}}</td>
                <td scope="row">{{$employee->company->name}}</td>
                <td>{{$created_at}}</td>
                <td>{{$updated_at}}</td>
                <td>
                    <a href="/employees/{{$employee->id}}/edit">ویرایش</a>||

                    <form action="employees/{{ $employee->id }}" method="post">
                        @csrf
                        {{ method_field('delete') }}
                        <button class="btn btn-default" type="submit">حذف</button>
                    </form>
                </td>

            </tr>

        </tbody>
    </table>




@endsection




