
@extends('master')

@section('content')



    <div class="row">
        <div class="col item_show">
            <div class="item_show_img_box">
                @foreach($article->media as $m)
                    @if($m->media_type_id==2)
                        <img src="/{{$m->path}}" alt="intro path" class="img-fluid">
                    @endif

                @endforeach
            </div>
            <div class="item_show_content_box">
                <h3>{{$article->title}}</h3>
                <p>{{$article->description}}</p>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h4>گالری تصاویر</h4>
            @foreach($article->media as $m)
                @if($m->media_type_id==3)

                    <img src="/{{$m->path}}" alt="full path" width="300px">
                @endif

            @endforeach
        </div>
    </div>

@endsection