@extends('admin_panel_master')

@section('content')

    @if (count($errors) > 0)
        <div class = "alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(['url' => '/employees/'.$employee->id,'method' => 'PUT','files' => true]) !!}
    {{ Form::hidden('id', $employee->id) }}
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        {{ Form::label('first_name', 'first_name')}}
                        {{Form::text('first_name', $employee->first_name,['class' => 'form-control','placeholder'=>'نام  را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('last_name', 'last_name')}}
                        {{Form::text('last_name', $employee->last_name,['class' => 'form-control','placeholder'=>'نام خانوادگی  را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('email', 'email')}}
                        {{Form::email('email', $employee->email,['class' => 'form-control','placeholder'=>'ایمیل  را وارد کنید'])}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phone', 'phone')}}
                        {{Form::number('phone', $employee->phone,['class' => 'form-control','placeholder'=>'تلفن  را وارد کنید'])}}
                    </div>


                    <div class="form-group">
                        {{ Form::label('company_id', 'company_id')}}
                        {!! Form::select('company_id', $items, $employee->company_id , ['class' => 'form-control']) !!}
                    </div>




                    {{Form::submit('Update employee')}}
                </div>
            </div>
        </div>

    </div>

    {!! Form::close() !!}


@endsection
