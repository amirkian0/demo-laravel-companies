<div class="row align-items-center">
    @foreach($articles as $article)
        <div class="col-4">
            <div class="article_box">
                <div class="article_box_image">
                    @foreach($article->media as $m)
                        @if($m->media_type_id==1)

                            <img src="/{{$m->path}}" class="img-fluid" alt="{{$m->title}}">
                        @endif
                    @endforeach

                </div>
                <div class="article_box_content p-3">
                    <h3 class="latest_article_heading my-2"><a href="/menu/articles/{{$article->id}}">{{$article->title}}</a></h3>
                    <p>{{$article->description}}</p>

                </div>
            </div>
        </div>
    @endforeach
</div>