<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>پروژه دمو کارمند شرکت</title>
    <!--add bootstrap framework-->
    <link rel="stylesheet" href="{{ url('asset/bootstrap/css/bootstrap.min.css') }}">
    <!--    <script src="bootstrap/js/jquery-slim.min.js"></script>
    -->
    <script src="{{ url('asset/bootstrap/js/jQueryv3.4.1.js') }}"></script>
    <script src="{{ url('asset/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ url('asset/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--End add bootstrap framework-->
    {{--my styles--}}
    <link rel="stylesheet" href="{{ url('asset/css/custom.css') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--End my styles--}}

</head>
<body class="app">
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
            </ul>
        </div>
    </div>
</nav>
<hr>
<main class="container">
    <div class="row">
        <div class="col-md-10">
            @yield('content')
        </div>

            <aside class="col-md-2">
                <ul class="list-group">
                    <a href=""><li class="list-group-item active">پنل مدیریت</li></a>
                    <a href="/companies"><li class="list-group-item">شرکت ها</li></a>
                    <a href="/employees"><li class="list-group-item">کارمندان</li></a>

                </ul>
            </aside>

    </div>

</main>
<footer></footer>
</body>
</html>


