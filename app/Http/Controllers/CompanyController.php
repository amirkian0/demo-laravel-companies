<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Symfony\Component\Console\Input\Input;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies=Company::paginate(10);


        return view('company.index')->with(compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation =  Validator::make($request->all(), [
            'name'   => 'required',
            'email'   => 'required|email',
            'logo_path'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'website'   => 'required',

        ]);
        if ($validation->fails()) {
            return redirect('companies/create')
                ->withErrors($validation)
                ->withInput(Input::all());
        }
        /***************************************************************/
        // for saving intro and full images
        if($request->hasFile('logo_path')) {
            $intro_img = $request->file('logo_path');
            //Move Uploaded File
            $destinationPath = 'asset/uploads/images/';
            $intro_img_name ="introImage".time().".".$intro_img->getClientOriginalExtension();
            $intro_img->move($destinationPath,$intro_img_name);
            $intro_img_path = $destinationPath . $intro_img_name;
        }
        /*************************************************************************/

        $company = new Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $curentTime=Carbon::now();
        $company->updated_at = $curentTime;
        $company->logo_path = $intro_img_path;
        $company->website = $request->website;
        if ($company->save()){

            return redirect('companies')->with('success','succeed');
        }
        else{
            return redirect('companies')->with('success','Unsucceed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company=Company::find($id);

        return view('company.show')->with(compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company=Company::find($id);


        return view('company.edit')->with(compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation =  Validator::make($request->all(), [
            'name'   => 'required',
            'email'   => 'required|email',
            'logo_path'   => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'website'   => 'required',

        ]);
        if ($validation->fails()) {
            return redirect('companies')
                ->withErrors($validation)
                ->withInput();
        }


        $company = Company::find($id);


        /*to checkout whether introimage updated or not*/
        if ($request->hasFile('logo_path')) {


                    // for saving intro and full images
                    if($request->hasFile('logo_path')) {
                        /*to remove exfile*/
                        if(file_exists($company->logo_path)) {
                            if(!unlink( $company->logo_path)){
                                die('coudnt delete intro_img_path');
                            }
                        }
                        /*End to remove exfile*/
                        $intro_img = $request->file('logo_path');
                        $destinationPath = 'asset/uploads/images/';
                        $intro_img_name ="introImage".time().".".$intro_img->getClientOriginalExtension();
                        $intro_img->move($destinationPath,$intro_img_name);
                        $intro_img_path = $destinationPath . $intro_img_name;

                    }



        }
        /*End to checkout whether introimage updated or not*/

        $curentTime=Carbon::now();
        $company->name=$request->input('name');
        $company->email=$request->input('email');
        $company->website=$request->input('website');
        $company->updated_at=$curentTime;
        $company->logo_path=$intro_img_path;
        if($company->save()){

            return redirect('companies')->with('success','updated');

        }else{
            return redirect('companies')->with('success','not updated');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //first we should delete physical media
        $company=Company::find($id);


            $mediapath=$company->logo_path;
            if(file_exists($mediapath)) {
                unlink($mediapath);
            }

//End first we should delete physical media

        $res=Company::where('id',$id)->delete();
        if($res){
            return redirect('companies')->with('success','succeed');
        }
        else{
            return redirect('companies')->with('success','unsucceed');
        }
    }
}
