<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Carbon;
use Symfony\Component\Console\Input\Input;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees=Employee::with('company')->paginate(15);

        return view('employee.index')->with(compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Company::pluck('name', 'id');
        return view('employee.create')->with(compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation =  Validator::make($request->all(), [
            'first_name'   => 'required',
            'last_name'   => 'required',
            'email'   => 'email',
            'phone'   => 'required',
            'company_id'   => 'required',

        ]);
        if ($validation->fails()) {
            return redirect('employees/create')
                ->withErrors($validation)
                ->withInput(Input::all());
        }
        /***************************************************************/

        $employee = new Employee();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->company_id = $request->company_id;
        $curentTime=Carbon::now();
        $employee->created_at = $curentTime;

        if ($employee->save()){

            return redirect('employees')->with('success','succeed');
        }
        else{
            return redirect('employees')->with('success','Unsucsses');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee=Employee::with('company')->find($id);
        return view('employee.show')->with(compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee=Employee::find($id);
        $items = Company::pluck('name', 'id');

        return view('employee.edit')->with(compact('employee','items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation =  Validator::make($request->all(), [
            'first_name'   => 'required',
            'last_name'   => 'required',
            'email'   => 'email',
            'phone'   => 'required',
            'company_id'   => 'required',

        ]);
        if ($validation->fails()) {
            return redirect('employees')
                ->withErrors($validation)
                ->withInput();
        }


        $employee = Employee::find($id);
        $employee->first_name=$request->input('first_name');
        $employee->last_name=$request->input('last_name');
        $employee->email=$request->input('email');
        $employee->phone=$request->input('phone');
        $employee->company_id=$request->input('company_id');
        $curentTime=Carbon::now();
        $employee->updated_at=$curentTime;
        if($employee->save()){

            return redirect('employees')->with('success','updated');

        }else{
            return redirect('employees')->with('success','not updated');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $res=Employee::where('id',$id)->delete();
        if($res){
            return redirect('employees')->with('success','succeed');
        }
        else{
            return redirect('employees')->with('success','unsucceed');
        }
    }
}
